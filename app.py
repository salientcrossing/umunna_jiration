# import datetime
# import uuid
# from cryptography.fernet import Fernet
# from cryptography.hazmat.backends import default_backend
# from cryptography.hazmat.primitives import hashes
# from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC

# class Patient:
#     def __init__(self, name, age, gender, condition, consent=False):
#         self.id = uuid.uuid4()
#         self.name = name
#         self.age = age
#         self.gender = gender
#         self.condition = condition
#         self.history = []
#         self.consent = consent

#     def update_condition(self, new_condition, consent=False):
#         if self.consent or consent:
#             self.condition = new_condition
#             self.history.append({'condition': new_condition, 'date': datetime.datetime.now()})
#         else:
#             raise Exception("Patient did not give consent for data sharing.")

#     def get_patient_info(self, password):
#         salt = b'salt_' #salt value
#         kdf = PBKDF2HMAC(
#             algorithm=hashes.SHA256,
#             length=32,
#             salt=salt,
#             iterations=100000,
#             backend=default_backend()
#         )
#         key = base64.urlsafe_b64encode(kdf.derive(password))
#         cipher_suite = Fernet(key)
#         ciphertext = cipher_suite.encrypt(str(self.__dict__).encode())
#         return ciphertext

# class MedicalRecord:
#     def __init__(self):
#         self.patients = {}

#     def add_patient(self, patient):
#         self.patients[patient.id] = patient

#     def update_patient_condition(self, patient_id, new_condition, consent=False):
#         self.patients[patient_id].update_condition(new_condition, consent)

