# import datetime
# import uuid
# import jwt
# from cryptography.fernet import Fernet
# from cryptography.hazmat.backends import default_backend
# from cryptography.hazmat.primitives import hashes, serialization
# from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
# from cryptography.hazmat.primitives.asymmetric import rsa

# class Patient:
#     def __init__(self, name, age, gender, condition, consent=False):
#         self.id = uuid.uuid4()
#         self.name = name
#         self.age = age
#         self.gender = gender
#         self.condition = condition
#         self.history = []
#         self.consent = consent
#         self._private_key = rsa.generate_private_key(
#             public_exponent=65537,
#             key_size=2048
#         )
#         self.public_key = self._private_key.public_key()

#     def update_condition(self, new_condition, consent=False):
#         if self.consent or consent:
#             self.condition = new_condition
#             self.history.append({'condition': new_condition, 'date': datetime.datetime.now()})
#         else:
#             raise Exception("Patient did not give consent for data sharing.")

#     def get_patient_info(self):
#         data = {
#             "id": self.id,
#             "name": self.name,
#             "age": self.age,
#             "gender": self.gender,
#             "condition": self.condition,
#             "history": self.history,
#             "consent": self.consent
#         }
#         return jwt.encode(data, self._private_key, algorithm='RS256')

# class MedicalRecord:
#     def __init__(self):
#         self.patients = {}
#         self._private_key = rsa.generate_private_key(
#             public_exponent=65537,
#             key_size=2048
#         )
#         self._public_key = self._private_key.public_key()
#         self._encryption_key = Fernet.generate_key()
#         self._cipher_suite = Fernet(self._encryption_key)

#     def add_patient(self, patient):
#         self.patients[patient.id] = patient
#         patient_info = patient.get_patient_info()
#         encrypted_patient_info = self._cipher_suite.encrypt(patient_info)
#         self.patients[patient.id]["info"] = encrypted_patient_info

#     def update_patient_condition(self, patient_id, new_condition, consent=False):
#         self.patients[patient_id].update_condition(new_condition, consent)
#         patient_info = self.patients[patient_id].get_patient_info()
#         encrypted_patient_info = self._cipher_suite.encrypt(patient_info)
#         self.patients[patient_id]["info"] = encrypted_patient_info
        
#     def get_patient_info(self, patient_id, private_key):
#         encrypted_patient_info = self.patients[patient_id][        "info"]
#         decrypted_patient_info = self._cipher_suite.decrypt(encrypted_patient_info)
#         patient_info = jwt.decode(decrypted_patient_info, private_key, algorithm='RS256')
#         return patient_info
    
#     def export_patient_data(self, patient_id, public_key):
#         patient_info = self.get_patient_info(patient_id, self._private_key)
#         patient_info['public_key'] = public_key.public_bytes(
#             encoding=serialization.Encoding.PEM,
#             format=serialization.PublicFormat.SubjectPublicKeyInfo
#         )
#         return patient_info

# # To instantiate a patient
# patient = Patient("John Doe", 30, "male", "healthy", consent=True)

# # To instantiate a medical record and add the patient to it
# med_record = MedicalRecord()
# med_record.add_patient(patient)

# # To update patient's condition
# med_record.update_patient_condition(patient.id, "fever", consent=True)

# # To export patient's data
# patient_data = med_record.export_patient_data(patient.id, patient.public_key)


