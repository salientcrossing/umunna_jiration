import bcrypt
import jwt
import datetime
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization, hashes
from cryptography.hazmat.primitives.asymmetric import rsa, padding

class Patient:
    def __init__(self, name, age, gender, condition):
        self.name = name
        self.age = age
        self.gender = gender
        self.condition = condition
        self.consent = False
        self.private_key = rsa.generate_private_key(
            public_exponent=65537,
            key_size=2048
        )
        self.public_key = self.private_key.public_key()

    def login(self, password):
        hashed_password = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt())
        # Compare the input password with the hashed password
        if bcrypt.checkpw(password.encode('utf-8'), hashed_password):
            return True
        else:
            return False

    def encrypt_data(self, data):
        public_key = self.public_key.public_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PublicFormat.SubjectPublicKeyInfo
        )
        recipient_key = RSA.import_key(public_key)
        ciphertext = recipient_key.encrypt(data, padding.OAEP(
            mgf=padding.MGF1(algorithm=hashes.SHA256()),
            algorithm=hashes.SHA256(),
            label=None))
        return ciphertext

    def decrypt_data(self, data):
        plaintext = self.private_key.decrypt(
            data,
            padding.OAEP(
                mgf=padding.MGF1(algorithm=hashes.SHA256()),
                algorithm=hashes.SHA256(),
                label=None)
        )
        return plaintext


class MedicalRecord:
    def __init__(self):
        self.patients = {}
        self.private_key = rsa.generate_private_key(
            public_exponent=65537,
            key_size=2048
        )
        self.public_key = self.private_key.public_key()
        self.providers = []

    def add_patient(self, patient):
        self.patients[patient.name] = patient

    def update_condition(self, patient_name, condition):
        self.patients[patient_name].condition = condition

    def retrieve_patient_data(self, patient_name, password):
        patient = self.patients[patient_name]
        if patient.login(password) and patient.consent:
            patient_data = {
                "name": patient.name,
                "age": patient.age,
                "gender": patient.gender,
                "condition": patient.condition
            }
            return patient_data
        else:
            return "Access denied"
        
    def add_provider(self, provider_public_key):
        self.providers.append(provider_public_key)

    def transmit_data(self, patient_name, patient_data):
        patient = self.patients[patient_name]
        encrypted_data = patient.encrypt_data(patient_data)
        for provider_public_key in self.providers:
            provider_public_key.encrypt(
                jwt.encode({patient_name: encrypted_data},
                           self.private_key, algorithm='RS256')
            )
            # The encrypted data is sent to the provider
            # The provider can then use their private key to decrypt the data

